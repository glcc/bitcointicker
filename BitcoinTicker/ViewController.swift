//
//  ViewController.swift
//  BitcoinTicker
//
//  Created by Gerson Costa on 23/10/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    let baseURL = "https://apiv2.bitcoinaverage.com/indices/global/ticker/BTC"
    let currencyArray = ["AUD", "BRL","CAD","CNY","EUR","GBP","HKD","IDR","ILS","INR","JPY","MXN","NOK","NZD","PLN","RON","RUB","SEK","SGD","USD","ZAR"]
    var finalURL = ""
    
    //Pre-setup IBOutlets
    @IBOutlet weak var bitcoinPriceLabel: UILabel!
    @IBOutlet weak var currencyPicker: UIPickerView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currencyPicker.delegate = self
        currencyPicker.dataSource = self
    }
    
    
    //TODO: Place your 3 UIPickerView delegate methods here
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencyArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let finalURL = baseURL + currencyArray[row]
        getBitcoinData(url: finalURL)
    }
    
    
    //MARK: - Networking
    /***************************************************************/

    func getBitcoinData(url: String) {

        Alamofire.request(url, method: .get).responseJSON { response in
            if response.result.isSuccess {

                let jsonData : JSON = JSON(response.result.value!)
                self.getBitcoinValue(json: jsonData)
            } else {
                
                print("Error: \(String(describing: response.result.error))")
                self.bitcoinPriceLabel.text = "Connection Issues"
            }
        }
    }

    //MARK: - JSON Parsing
    /***************************************************************/

    func getBitcoinValue(json : JSON) {
        
        if let tempResult = json["open"]["day"].double {
            updateBitcoinLabel(value: tempResult)
        } else {
            bitcoinPriceLabel.text = "Data unavailable. Please try again"
        }
    }
    
    //MARK: - UI Updates
    func updateBitcoinLabel(value: Double) {
        bitcoinPriceLabel.text = String(value)
    }
}

